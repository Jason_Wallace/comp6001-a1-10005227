function changeBackground() {
	
	// made variables to get the values from the sliders
    var rd = parseInt(document.getElementById('red').value);
	var gn = parseInt(document.getElementById('green').value);
	var bl = parseInt(document.getElementById('blue').value);
	
	// converted the decimal values into hexadecimal
    var rdhex = (rd < 16) ? "0" + rd.toString(16) : rd.toString(16);
	var gnhex = (gn < 16) ? "0" + gn.toString(16) : gn.toString(16);
	var blhex = (bl < 16) ? "0" + bl.toString(16) : bl.toString(16);
	
    
	// concatenated variables
	var hexcode = "#" + rdhex + gnhex + blhex;
	
    // changed the background color
    document.body.style.backgroundColor = hexcode;
	document.getElementById('hexdisplay').innerHTML = hexcode;
	
	} 

    //sliders go back to original position
    function resetSlider() {
    document.body.style.backgroundColor = "white";
 
}
