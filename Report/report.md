#Assignment 1 Report
##10005227 - Jason Wallace

###Introduction - ASP.NET

###There are many GUI best practices for ASP.NET, although many of them appear to be beyond the scope of COMP6001. I have outlined several key points of GUI best practices.

###Controllers

###Deleting demo code files is important since they are default, and not needed(Kanjinal, 2015). Controllers should be as efficient as possible, and away from data access, since the controller renders different views depending on context (Kanjinal, 2015). There should be a layer between the controller and repository (Esposito, n.d). Folders typically group controllers used by applications (Esposito, n.d). Controllers need to be free of SQL Statements or HTML, since they are not suitable in context(Best MVC Practices, n.d). 


###Minifying

###CSS and script files should be bundled and minified. This involves removing unnecessary elements of code such as characters, comments and white space (Kanjinal, 2015). This should also improve page load times (Shekhawat, 2016). Some examples are as follows:

```public static void RegisterBundles(

                       BundleCollection bundles)



   bundles.Add(new StyleBundle("~/Content/Styles")

       .Include("~/Content/Styles/bootstrap.css",

                "~/Content/Styles/example1.css"));


```

  
 ``` .Include(

     "~/Content/Scripts/example2.js",

     "~/Content/Scripts/knockout-3.0.0.js")

);
```

###Caching

###This can help improve performance by storing unused data in memory, and ease network congestion (Kanjinal, 2012). Caching should be done often for all parts of applications (Kanjinal, 2015) Caching in controllers can occur like this:

```public class IDGController : Controller



    [OutputCache(Duration=3600,

VaryByParam="none")]

    public ActionResult Index()

    {

    }


```
###Disable Request Validation

###Although Request Validation potentially stops compromising content, in certain cases, sending content to server may be needed (Kanjinal, 2012). This code demonstrates how:

```[ValidateInput(false)]
[AcceptVerbs(HttpVerbs.Post)]
public ActionResult Create([Bind(Exclude="Id")]Employee empObj)
{
```

###Master View Model

###Master Pages are frequently used in ASP.NET applications, and extended in order to give consistency to an application (Kanjinal, 2012). A Master View Model needs to be created, as follows:

    
    ```public class ViewModelBase
    {
        public ViewModelBase()
        {
     
        }
   
    }```

###Annotations

###Annotations help in validation, by specifying data attributes, that create the data rules (Kanjinal, 2012). It makes code easier to write and test, and enables clarification of classes and its members (Appel, 2015). 

###Views

###Views should contain mainly HTML and PHP to represent data, not code like databases (Best MVC Practices, n.d). Direct controller access from the view should be for presentation use only (Best MVC Practices, n.d).                                     

###ViewState

###ViewState is automatically turned on in ASP.NET, although it is not always necessary ("10 Best Practices for ASP.NET Developers", n.d). ViewState can be used to store data, and does not impose timeouts. However, ViewStates are not recommended for critical data, multiple page information storage, or big data ("ViewState in ASP.NET 2.0")

###Peer review

###This report was well written, very concise and well researched/referenced. It contains all 8 of the required points and they are relevant to the topic and easy to understand. Markdown was used correctly. A slight more detail could be useful though, as some sections were not unclear, but could be explained better. That said, each link was easy to follow and led me to the correct information on the webpages, so I feel that this report has been a success. - Jarred Land


###References

###Kanjinal, J. (2015). _Best practices in ASP.Net MVC_ . Retrieved from http://www.infoworld.com/article/2941414/microsoft-net/best-practices-in-asp-net-mvc.html

###Esposito, L.(n.d). _10 Good Practices for ASP.NET MVC Apps_. Retrieved from http://www.codemag.com/Article/1405071

###_Best MVC Practices_.(n.d). Retrieved 6th August, 2017 from http://www.yiiframework.com/doc/guide/1.1/en/basics.best-practices 

###Singh,S.(2016). _ASP.NET MVC Performance: Bundling And Minification_. Retrieved from http://www.c-sharpcorner.com/article/asp-net-mvc-performance-bundling-and-minification/ 

###Kanjinal, J.(2012). _Top 10 ASP.NET MVC Best Practices_. Retrieved from http://www.codeguru.com/csharp/.net/net_asp/mvc/top-10-asp.net-mvc-best-practices.htm 

###Kanjinal, J.(2015). _Best practices in caching in ASP.Net_. Retrieved from http://www.infoworld.com/article/2976934/application-architecture/best-practices-in-caching-in-aspnet.html 

###_10 Best Practices for ASP.NET Developers_.(2017). Retrieved from http://opalforce.com/10-best-practices-for-asp-net-developers/

###Appel, R. (2015). _How data annotations for ASP.NET MVC validation work_. Retrived from http://rachelappel.com/asp-net-mvchow-data-annotations-for-asp-net-mvc-validation-work/ 

###_ViewState in ASP.NET 2.0_. (n.d). Retrieved from http://www.beansoftware.com/ASP.NET-Tutorials/ViewState-In-ASP.NET.aspx


